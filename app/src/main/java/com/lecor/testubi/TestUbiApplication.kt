package com.lecor.testubi

import android.app.Application
import com.lecor.testubi.repository.LocationRepository
import com.lecor.testubi.viewmodel.SpeedViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module

class TestUbiApplication: Application() {

    override fun onCreate() {
        super.onCreate()

        initializeInjection()
    }

    private fun initializeInjection(){
        val myViewModels = module{
            viewModel{ SpeedViewModel(get()) }
            single { LocationRepository(applicationContext) }
        }

        startKoin {
            androidContext(this@TestUbiApplication)
            modules(myViewModels)
        }
    }
}