package com.lecor.testubi.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.lecor.testubi.R
import com.lecor.testubi.viewmodel.SpeedViewModel
import kotlinx.android.synthetic.main.fragment_speed_average.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class AverageSpeedFragment : Fragment() {
    private val speedViewModel: SpeedViewModel by viewModel()
    private val args: AverageSpeedFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_speed_average, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        speed_average.text = getString(R.string.average_speed, args.speedAverage)

        speedViewModel.getCurrentLocation()
            .observe(viewLifecycleOwner, Observer { currentLocation ->
                currentLocation?.let {
                    val speed=it.speed*3.6f
                    if(speed>1){
                        findNavController().navigate(AverageSpeedFragmentDirections.actionAverageSpeedFragmentToSpeedFragment())
                    }
                }
            })
    }
}