package com.lecor.testubi.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.lecor.testubi.R
import com.lecor.testubi.viewmodel.SpeedViewModel
import kotlinx.android.synthetic.main.fragment_speed.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class SpeedFragment :Fragment(){
    private val speedViewModel: SpeedViewModel by viewModel()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_speed, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        speedViewModel.getCurrentLocation().observe(viewLifecycleOwner, Observer { currentLocation ->
            currentLocation?.let {
                val speed=it.speed*3.6f
                speed_meter.text=getString(R.string.actual_speed,speed)
                if(speed>=1){
                    speedViewModel.addSpeedSave(speed)
                }else{
                    findNavController().navigate(SpeedFragmentDirections.actionSpeedFragmentToAverageSpeedFragment(speedViewModel.getAverageSpeed()))
                }
            }
        })
    }
}