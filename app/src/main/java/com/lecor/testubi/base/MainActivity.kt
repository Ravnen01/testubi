package com.lecor.testubi.base

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.lecor.testubi.R
import com.lecor.testubi.repository.LocationRepository
import org.koin.android.ext.android.inject


class MainActivity : AppCompatActivity() {

    val locationRepository: LocationRepository by inject()

    val MY_PERMISSIONS_REQUEST_LOCATION = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        checkPermission()

    }

    fun checkPermission() {
        if (ContextCompat.checkSelfPermission(
                baseContext,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            ) {
                showExplication();
            } else {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf( Manifest.permission.ACCESS_FINE_LOCATION ),
                    MY_PERMISSIONS_REQUEST_LOCATION
                )
            }
        } else {
            locationRepository.launchLocationManager()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_LOCATION -> {
                if (grantResults.isNotEmpty()
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                ) {
                    locationRepository.launchLocationManager()
                } else {
                    showExplication()
                }
            }
        }
    }

    private fun showExplication() {
        AlertDialog.Builder(this)
            .setTitle(R.string.location_dialog_title)
            .setMessage(R.string.location_dialog_information)
            .setPositiveButton(
                android.R.string.yes
            ) { _, _ ->
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    MY_PERMISSIONS_REQUEST_LOCATION
                )
            }
            .setNegativeButton(
                android.R.string.no
            ) { _, _ -> finish() }
            .setCancelable(false)
            .create().show()
    }


}
