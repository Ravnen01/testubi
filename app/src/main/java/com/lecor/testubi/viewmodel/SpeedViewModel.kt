package com.lecor.testubi.viewmodel

import android.location.Location
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.lecor.testubi.repository.LocationRepository

class SpeedViewModel(val locationRepository: LocationRepository) : ViewModel() {

    private val tempSaveSpeed = ArrayList<Float>()
    private var averageSpeed = 0f
    private val currentLocation = locationRepository.currentLocation

    fun getCurrentLocation(): LiveData<Location> {
        return currentLocation
    }

    fun addSpeedSave(speed: Float) {
        tempSaveSpeed.add(speed)
        if (tempSaveSpeed.size == 10) {
            updateAverageSpeed()
        }
    }

    private fun updateAverageSpeed() {
        var sumSpeed = 0f
        for (speed in tempSaveSpeed) {
            sumSpeed += speed
        }
        averageSpeed = if (averageSpeed == 0f) {
            sumSpeed / tempSaveSpeed.size
        } else {
            (averageSpeed + sumSpeed) / (tempSaveSpeed.size + 1)
        }
        tempSaveSpeed.clear()
    }

    fun getAverageSpeed(): Float {
        if (tempSaveSpeed.isNotEmpty())
            updateAverageSpeed()
        return averageSpeed
    }


}